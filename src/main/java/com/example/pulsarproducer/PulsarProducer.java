package com.example.pulsarproducer;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class PulsarProducer {
    private final LinkedBlockingQueue<String> queue;
    private Logger logger = LoggerFactory.getLogger(PulsarProducer.class);
    private final PulsarClient pulsarClient;
    private ExecutorService executorService;
    private Future future;

    @Autowired
    public PulsarProducer(PulsarClient pulsarClient, QueueHolder queueHolder){
        this.pulsarClient = pulsarClient;
        executorService = Executors.newFixedThreadPool(1);
        this.queue = queueHolder.getQueue();
    }

    @PostConstruct
    private void init() {
        future = executorService.submit(this::generate);
    }

    private void generate() {
        try {
            logger.info("pulsar generator starts");
            Producer<byte[]> producer = createPulsarProducer();

            while (!Thread.currentThread().isInterrupted()) {
                String msg = queue.take();
                produceEvents(producer, msg);

            }
        } catch (IOException | InterruptedException ex) {
            logger.error("exception", ex);
        }

        logger.info("pulsar generator exit");
    }

    private void produceEvents(Producer<byte[]> producer, String msg) throws IOException {
        logger.info("produce event: " + msg);
        producer.send(msg.getBytes());
    }

    @PreDestroy
    private void preDestroy(){
        logger.info("killing consumer");
        future.cancel(true);
    }

    private Producer<byte[]> createPulsarProducer() throws PulsarClientException {
        return pulsarClient.newProducer().enableBatching(true).blockIfQueueFull(true)
                .topic(System.getenv("TARGET_TOPIC")).create();
    }
}


