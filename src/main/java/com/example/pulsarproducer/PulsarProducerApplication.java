package com.example.pulsarproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PulsarProducerApplication {
/*
PULSAR_BROKER_URL=pulsar://localhost:6650
TARGET_TOPIC=test31
*/

/*
AWS:
PULSAR_BROKER_URL=pulsar+ssl://pulsar-broker.dev.eng.forescoutcloud.net:6651 \
PULSAR_AUTH_PLUGIN=org.apache.pulsar.client.impl.auth.AuthenticationTls \
PULSAR_AUTH_PARAMS=tlsCertFile:eyecloud.pem,tlsKeyFile:eyecloud.pk8.key.pem \
TARGET_TOPIC=persistent://internal/eyecloud/test12 \
java -jar pulsar-producer-0.0.1-SNAPSHOT.jar
 */
public static void main(String[] args) {
		SpringApplication.run(PulsarProducerApplication.class, args);
	}
}
