package com.example.pulsarproducer;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.concurrent.LinkedBlockingQueue;

@Component
@Getter
public class QueueHolder {
    private LinkedBlockingQueue<String> queue = new LinkedBlockingQueue();
}
