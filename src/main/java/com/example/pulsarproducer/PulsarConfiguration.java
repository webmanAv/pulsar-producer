package com.example.pulsarproducer;

import org.apache.pulsar.client.api.ClientBuilder;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.shade.com.google.common.base.Strings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PulsarConfiguration {
    private PulsarClient createPulsarClient() throws PulsarClientException {
        String brokerUrl = System.getenv("PULSAR_BROKER_URL");
        ClientBuilder clientBuilder = PulsarClient.builder().serviceUrl(brokerUrl);
        String authPlugin = System.getenv("PULSAR_AUTH_PLUGIN");
        if (!Strings.isNullOrEmpty(authPlugin)){
            String authParams = System.getenv("PULSAR_AUTH_PARAMS");
            clientBuilder.authentication(authPlugin, authParams);
        }

        return clientBuilder.build();
    }

    @Bean
    PulsarClient pulsarClient() throws PulsarClientException {
        return createPulsarClient();
    }
}