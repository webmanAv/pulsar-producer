package com.example.pulsarproducer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.LinkedBlockingQueue;

@RestController
public class ProducerController {
    private final LinkedBlockingQueue<String> queue;

    @Autowired
    public ProducerController(QueueHolder queueHolder) {
        this.queue = queueHolder.getQueue();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void postData(@RequestBody String body) throws InterruptedException {
        String[] lines = body.split("\\r?\\n");
        for (String line : lines){
            queue.put(line);
        }
    }
}
